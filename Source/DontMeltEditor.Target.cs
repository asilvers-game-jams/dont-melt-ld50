using UnrealBuildTool;
using System.Collections.Generic;

public class DontMeltEditorTarget : TargetRules
{
	public DontMeltEditorTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "DontMelt" } );
	}
}
