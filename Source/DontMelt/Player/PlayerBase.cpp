#include "PlayerBase.h"

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "../Game/GameData.h"
#include "../Game/GameSettings.h"
#include "DontMelt/Characters/MerchantBase.h"

APlayerBase::APlayerBase()
{
	CameraArmComponent = CreateDefaultSubobject<USpringArmComponent>("Camera Arm");
	CameraArmComponent->TargetArmLength = 400;
	CameraArmComponent->SetupAttachment(RootComponent);
	CameraArmComponent->bUsePawnControlRotation = false;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>("Camera");
	CameraComponent->bUsePawnControlRotation = true;
	CameraComponent->SetupAttachment(CameraArmComponent, CameraArmComponent->SocketName);
}

void APlayerBase::Heal(float amount)
{
	Health += amount;
	SetHealthScale();

	if(Health < 0)
	{
		SetActorScale3D(FVector(1, 1, 1));
		OnDeath(false);
	}
}

void APlayerBase::StartShopping(AMerchantBase* merchant)
{
	if (IsFrozen() || !merchant) return;

	IsShopping = true;
	ShoppingWith = merchant;
	OnStartShopping(merchant);
}

void APlayerBase::CompleteShopping(bool boughtItem)
{
	if (!IsShopping) return;

	IsShopping = false;
	if(boughtItem)
	{
		ShoppingWith->BuyItem(this);
	}
	ShoppingWith = null;

	if(!HasTicket)	OnStopShopping();
}

void APlayerBase::BeginPlay()
{
	Super::BeginPlay();

	GetPlayerController()->PlayerCameraManager->ViewPitchMax = MaxPitch;
	GetPlayerController()->PlayerCameraManager->ViewPitchMin = -MaxPitch;

	GetCharacterMovement()->MaxWalkSpeed = MaxWalkSpeed;
	GetCharacterMovement()->JumpZVelocity = JumpVelocity;
	GetCharacterMovement()->GravityScale = FallingGravityScale;
	GetCharacterMovement()->MaxAcceleration = Acceleration;
	GetCharacterMovement()->BrakingDecelerationWalking = Deceleration;
	GetCharacterMovement()->bOrientRotationToMovement = true;

	Health = StartingHealth;
}

APlayerController* APlayerBase::GetPlayerController()
{
	return static_cast<APlayerController*>(Controller);
}

void APlayerBase::Move(float amount, EAxis::Type axis)
{
	auto rot = Controller->GetControlRotation();
	auto yaw = FRotator(0, rot.Yaw, 0);

	auto dir = FRotationMatrix(yaw).GetUnitAxis(axis);
	AddMovementInput(dir, amount);
}

void APlayerBase::MoveForward(float amount)
{
	if (IsFrozen()) return;

	Move(amount, EAxis::X);
}

void APlayerBase::MoveRight(float amount)
{
	if (IsFrozen()) return;

	Move(amount, EAxis::Y);
}

void APlayerBase::StartJump()
{
	if (IsFrozen()) return;

	Jumping = true;
	CanStopJumping = false;

	var movement = GetCharacterMovement();
	movement->GravityScale = JumpGravityScale;
	Jump();
}

void APlayerBase::EndJump()
{
	Jumping = false;
	if(CanStopJumping)
	{
		ResetJumping();
	}
}

void APlayerBase::CompleteJumpTimeout()
{
	CanStopJumping = true;
	if(!Jumping)
	{
		ResetJumping();
	}
}

void APlayerBase::CheckFalling()
{
	if(GetVelocity().Z <= 0)
	{
		Jumping = false;
		ResetJumping();
	}
}

void APlayerBase::ResetJumping()
{
	GetCharacterMovement()->GravityScale = FallingGravityScale;
}

void APlayerBase::LookUp(float amount)
{
	if (IsFrozen()) return;

	var result = amount * BaseTurnRate * GetWorld()->GetDeltaSeconds();
	result *= global_settings->CameraSensitivity;
	if (global_settings->InvertCameraY) result *= -1;

	AddControllerPitchInput(result);
}

void APlayerBase::TurnCamera(float amount)
{
	if (IsFrozen()) return;

	var result = amount * BaseTurnRate * GetWorld()->GetDeltaSeconds();
	result *= global_settings->CameraSensitivity;
	if (global_settings->InvertCameraX) result *= -1;

	AddControllerYawInput(result);
}

void APlayerBase::HandleTick(float deltaSeconds)
{
	if(GetActorLocation().Z < -2500)
	{
		SetActorLocation(FVector(0, 0, 100));
	}

	if (IsFrozen()) return;

	if (Jumping) CheckFalling();

	var lossAmt = NormalHealthLostSpeed;
	if(HasSunGlasses) lossAmt *= SunglassesFactor;
	if (HasHat) lossAmt *= HatFactor;

	var healthLost = lossAmt * deltaSeconds;
	
	Health -= healthLost;

	if(Health < 0)
	{
		SetActorScale3D(FVector(1, 1, 1));
		OnDeath(false);
	}
	else
	{
		SetHealthScale();
	}

	SecondsPlayed += deltaSeconds;
}

void APlayerBase::SetHealthScale()
{
	var scaleAmt = FMath::GetMappedRangeValueClamped(FVector2D(0, 2 * StartingHealth), FVector2D(0.5, 3), Health);
	SetActorScale3D(FVector(scaleAmt, scaleAmt, scaleAmt));
}

void APlayerBase::AddMoney(int amount)
{
	Money += amount;
	OnMoneyChange(Money);
}

void APlayerBase::SpendMoney(int amount)
{
	Money -= amount;
	OnMoneyChange(Money);
}

int APlayerBase::GetMoney()
{
	return Money;
}

FString APlayerBase::GetTimePlayedString()
{
	var totalSeconds = FMath::CeilToInt(SecondsPlayed);
	var seconds = totalSeconds % 60;
	var minutes = totalSeconds / 60;

	return FString::FromInt(minutes) + ":" + (seconds < 10 ? "0" : "") + FString::FromInt(seconds);
}

bool APlayerBase::IsFrozen()
{
	return IsPaused || IsShopping || Health < 0 || HasTicket;
}

void APlayerBase::BoughtHat()
{
	if (HasHat) return;

	HasHat = true;
}

void APlayerBase::BoughtSunGlasses()
{
	if (HasSunGlasses) return;

	HasSunGlasses = true;
}

void APlayerBase::BoughtShoes()
{
	if (HasShoes) return;

	HasShoes = true;
	GetCharacterMovement()->MaxAcceleration = MaxWalkSpeed * 2;
	GetCharacterMovement()->BrakingDecelerationWalking = MaxWalkSpeed * 4;
}

void APlayerBase::BoughtTicket()
{
	HasTicket = true;
	OnDeath(true);
}


float APlayerBase::GetHealthPercent()
{
	if (Health < 0) return 0;
	else if (Health > StartingHealth) return 1;

	return Health / StartingHealth;
}
