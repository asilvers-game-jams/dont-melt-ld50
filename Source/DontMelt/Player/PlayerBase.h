#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../General/CoreGeneral.h"

#include "PlayerBase.generated.h"

class AMerchantBase;
class UCameraComponent;
class USpringArmComponent;
UCLASS()
class DONTMELT_API APlayerBase : public ACharacter
{
	GENERATED_BODY()

public:
	APlayerBase();
	
	UFUNCTION(BlueprintCallable, Category = "Tick")
	void Heal(float amount);

	void StartShopping(AMerchantBase* merchant);

	UFUNCTION(BlueprintCallable, Category="Shopping")
	void CompleteShopping(bool boughtItem);
	
	UFUNCTION(BlueprintCallable, Category="Money")
	void SpendMoney(int amount);

	UFUNCTION(BlueprintCallable, Category="Money")
	int GetMoney();

protected:
	virtual void BeginPlay() override;

	APlayerController* GetPlayerController();

	void Move(float amount, EAxis::Type axis);
	UFUNCTION(BlueprintCallable, Category="Movement")
	void MoveForward(float amount);
	UFUNCTION(BlueprintCallable, Category="Movement")
	void MoveRight(float amount);

	UFUNCTION(BlueprintCallable, Category="Movement")
	void StartJump();
	UFUNCTION(BlueprintCallable, Category="Movement")
	void EndJump();
	UFUNCTION(BlueprintCallable, Category="Movement")
	void CompleteJumpTimeout();
	UFUNCTION(BlueprintCallable, Category="Movement")
	void CheckFalling();
	void ResetJumping();

	UFUNCTION(BlueprintCallable, Category="Camera")
	void LookUp(float amount);
	
	UFUNCTION(BlueprintCallable, Category="Camera")
	void TurnCamera(float amount);

	UFUNCTION(BlueprintCallable, Category="Tick")
	void HandleTick(float deltaSeconds);

	void SetHealthScale();

	UFUNCTION(BlueprintImplementableEvent, Category = "Money")
	void OnMoneyChange(int currentAmount);

	UFUNCTION(BlueprintImplementableEvent, Category = "Money")
	void OnStartShopping(AMerchantBase* with);

	UFUNCTION(BlueprintImplementableEvent, Category = "Money")
	void OnStopShopping();

	UFUNCTION(BlueprintImplementableEvent, Category = "Death")
	void OnDeath(bool won);

	UFUNCTION(BlueprintCallable, Category="Money")
	void AddMoney(int amount);

	UFUNCTION(BlueprintCallable, Category="Time")
	FString GetTimePlayedString();

	UFUNCTION(BlueprintCallable, Category="Pause")
	bool IsFrozen();

	UFUNCTION(BlueprintCallable, Category="Items")
	void BoughtHat();

	UFUNCTION(BlueprintCallable, Category = "Items")
	void BoughtSunGlasses();

	UFUNCTION(BlueprintCallable, Category = "Items")
	void BoughtShoes();

	UFUNCTION(BlueprintCallable, Category = "Items")
	void BoughtTicket();

	UFUNCTION(BlueprintCallable, Category = "Health")
	float GetHealthPercent();

protected:

	UPROPERTY(EditAnywhere)
	USpringArmComponent* CameraArmComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UCameraComponent* CameraComponent;
	
	UPROPERTY(EditAnywhere, Category="Camera")
	float BaseTurnRate {45};

	UPROPERTY(EditAnywhere, Category="Camera")
	float MaxPitch {55};

	UPROPERTY(EditAnywhere, Category="Movement")
	float JumpVelocity {500};

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Movement")
	float MinimumJumpTime {0.5};

	UPROPERTY(EditAnywhere, Category="Movement|Gravity")
	float JumpGravityScale {1};
	
	UPROPERTY(EditAnywhere, Category="Movement|Gravity")
	float FallingGravityScale {1.25};

	UPROPERTY(EditAnywhere, Category="Movement")
	float MaxWalkSpeed {100};

	UPROPERTY(EditAnywhere, Category="Movement")
	float Acceleration {100};
	
	UPROPERTY(EditAnywhere, Category="Movement")
	float Deceleration {100};

	UPROPERTY(BlueprintReadOnly, Category="Movement")
	bool Jumping {false};
	bool CanStopJumping {true};

	UPROPERTY(EditAnywhere, Category="Health")
	float StartingHealth {60};

	UPROPERTY(EditAnywhere, Category = "Health")
	float MinScale {0.1};

	UPROPERTY(EditAnywhere, Category="Health")
	float NormalHealthLostSpeed {1};

	UPROPERTY(EditAnywhere, Category="Health")
	float SunglassesFactor {0.666};

	UPROPERTY(EditAnywhere, Category="Health")
	float HatFactor {0.75};

	UPROPERTY(BlueprintReadWrite, Category="Pause")
	bool IsPaused {false};

	UPROPERTY(BlueprintReadWrite, Category="Pause")
	bool IsShopping {false};

	float SecondsPlayed {0};

	float Health {100};

	float Money {0};

	bool HasHat {false};
	bool HasSunGlasses {false};
	bool HasShoes {false};

	UPROPERTY(BlueprintReadWrite, Category = "Win")
	bool HasTicket {false};

	UPROPERTY()
	AMerchantBase* ShoppingWith;
};
