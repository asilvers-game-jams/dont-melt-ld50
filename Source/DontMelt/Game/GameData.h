#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "../General/CoreGeneral.h"

#include "GameData.generated.h"

class UGameSettings;
UCLASS()
class DONTMELT_API UGameData : public UGameInstance
{
	GENERATED_BODY()

public:
	UGameData();
	~UGameData();

	UFUNCTION(BlueprintCallable, Category="Game Data", DisplayName="Get Game Data")
	static UGameData* Instance();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category="Save Data")
	void SaveSettings();

	UPROPERTY(BlueprintReadWrite, Category="Save Data")
	UGameSettings* GameSettings;

protected:
	static UGameData* _instance;
};
