#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "GameSettings.generated.h"

UCLASS()
class DONTMELT_API UGameSettings : public USaveGame
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, Category="Save Data")
	int HighScoreSeconds {-1};

	UPROPERTY(BlueprintReadWrite, Category = "Save Data")
	bool InvertCameraY {false};

	UPROPERTY(BlueprintReadWrite, Category = "Save Data")
	bool InvertCameraX {false};

	UPROPERTY(BlueprintReadWrite, Category = "Save Data")
	float CameraSensitivity {1};

	UPROPERTY(BlueprintReadWrite, Category = "Save Data")
	float VolumeAdjustment {1};
};
