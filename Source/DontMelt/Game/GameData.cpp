#include "GameData.h"

UGameData* UGameData::_instance = null;

UGameData::UGameData()
{
	_instance = this;
}

UGameData::~UGameData()
{
	if(_instance == this)
	{
		_instance = null;
	}
}

UGameData* UGameData::Instance()
{
	return _instance;
}
