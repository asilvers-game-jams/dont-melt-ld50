#include "PathCollectableBase.h"

void APathCollectableBase::Step(float deltaSeconds)
{
	if (WorldPath.Num() <= 1) return;

	var next = WorldPath[PathIndex];
	var actorLoc = GetActorLocation();
	var direction = (next - actorLoc).Rotation();
	direction.Normalize();

	var currentDirection = GetActorRotation();
	var directionDiff = direction - currentDirection;
	if(directionDiff.IsNearlyZero())
	{
		// Move Towards the next point
		var moveAmount = deltaSeconds * MovementSpeed;
		var diff = (next - actorLoc).Length();
		if(FMath::Abs(diff) < FMath::Abs(moveAmount))
		{
			SetActorLocation(next);
			PathIndex += (!ReverseAtEnd || Forward) ? 1 : -1;
			if(PathIndex >= WorldPath.Num() || PathIndex < 0)
			{
				if(ReverseAtEnd)
				{
					Forward = !Forward;
					PathIndex = Forward ? 1 : WorldPath.Num() - 2;
				}
				else
				{
					PathIndex = 0;
				}
			}
		}
		else
		{
			var addLoc = currentDirection.Vector() * moveAmount;
			AddActorWorldOffset(addLoc);
		}
	}
	else
	{
		// Rotate Towards the next point
		var remaining = directionDiff.Yaw;
		var turnAmount = deltaSeconds * TurningSpeed;
		if(FMath::Abs(turnAmount) > FMath::Abs(remaining))
		{
			AddActorWorldRotation(FRotator(0, remaining, 0));
		}
		else
		{
			AddActorWorldRotation(FRotator(0, turnAmount * (remaining < 0 ? -1 : 1), 0));
		}
	}
}

void APathCollectableBase::BeginPlay()
{
	Super::BeginPlay();

	var actorLocation = GetActorLocation();
	var actorRotation = GetActorRotation();
	WorldPath.Empty();
	for(var i = 0; i < Path.Num(); i++)
	{
		var loc = Path[i];
		WorldPath.Add(actorLocation + actorRotation.RotateVector(loc));
	}
}
