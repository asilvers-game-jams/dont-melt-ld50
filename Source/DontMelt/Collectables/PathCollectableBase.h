#pragma once

#include "CoreMinimal.h"
#include "CollectableBase.h"
#include "PathCollectableBase.generated.h"

UCLASS()
class DONTMELT_API APathCollectableBase : public ACollectableBase
{
	GENERATED_BODY()

protected:
	UFUNCTION(BlueprintCallable, Category="Pathing")
	void Step(float deltaSeconds);

	virtual void BeginPlay() override;

protected:
	UPROPERTY(EditAnywhere, Category="Pathing", Meta=(MakeEditWidget = true))
	TArray<FVector> Path;

	UPROPERTY(EditAnywhere, Category="Pathing")
	float MovementSpeed {45};

	UPROPERTY(EditAnywhere, Category="Pathing")
	float TurningSpeed {45};

	UPROPERTY(EditAnywhere, Category="Pathing")
	bool ReverseAtEnd {true};
	
	TArray<FVector> WorldPath;
	int PathIndex {1};

	bool Forward {true};
};
