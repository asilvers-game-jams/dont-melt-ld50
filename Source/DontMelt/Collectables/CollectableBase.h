

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../General/CoreGeneral.h"

#include "CollectableBase.generated.h"

class APlayerBase;
class USphereComponent;
UCLASS()
class DONTMELT_API ACollectableBase : public AActor
{
	GENERATED_BODY()
	
public:
	ACollectableBase();

	UFUNCTION()
	void OverlapPickupArea(UPrimitiveComponent* overlappedComponent, AActor* otherActor, UPrimitiveComponent* otherComponent, int otherBodyIndex, bool fromSweep, const FHitResult& hitResult);

	UFUNCTION(BlueprintImplementableEvent, Category="Events")
	void OnPlayerPickUp(APlayerBase* player);

protected:
	virtual void BeginPlay() override;

public:	

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USphereComponent* PickupArea;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* Mesh;
};
