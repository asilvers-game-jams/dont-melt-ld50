#include "CollectableBase.h"

#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "DontMelt/Player/PlayerBase.h"

ACollectableBase::ACollectableBase()
{
	if(!RootComponent)
	{
		RootComponent = CreateDefaultSubobject<USceneComponent>("Root");
	}

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Mesh->SetupAttachment(RootComponent);

	PickupArea = CreateDefaultSubobject<USphereComponent>("Pickup Area");
	PickupArea->SetupAttachment(Mesh);
	PickupArea->InitSphereRadius(100);
	PickupArea->OnComponentBeginOverlap.AddDynamic(this, &ACollectableBase::OverlapPickupArea);
}

void ACollectableBase::OverlapPickupArea(UPrimitiveComponent* overlappedComponent, AActor* otherActor,
	UPrimitiveComponent* otherComponent, int otherBodyIndex, bool fromSweep, const FHitResult& hitResult)
{
	var player = dynamic_cast<APlayerBase*>(otherActor);
	if(player)
	{
		OnPlayerPickUp(player);
	}
}

void ACollectableBase::BeginPlay()
{
	Super::BeginPlay();
}
