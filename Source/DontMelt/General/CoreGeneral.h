#pragma once

#include "CoreMinimal.h"

// Muscle Memory from C#
#define var auto
#define null nullptr

#define global_settings UGameData::Instance()->GameSettings

#define debug(msg) GEngine->AddOnScreenDebugMessage(-1, 1000, FColor::Cyan, msg);
#define debugi(msg, id) GEngine->AddOnScreenDebugMessage(id, 1000, FColor::Cyan, msg);