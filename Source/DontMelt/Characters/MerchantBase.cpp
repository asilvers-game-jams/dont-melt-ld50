#include "MerchantBase.h"

#include "Components/SphereComponent.h"
#include "DontMelt/Player/PlayerBase.h"

AMerchantBase::AMerchantBase()
{
	if(!RootComponent)
	{
		RootComponent = CreateDefaultSubobject<USceneComponent>("Root");
	}

	InteractArea = CreateDefaultSubobject<USphereComponent>("Interact Area");
	InteractArea->SetupAttachment(RootComponent);
	InteractArea->InitSphereRadius(100);
	InteractArea->OnComponentBeginOverlap.AddDynamic(this, &AMerchantBase::OverlapInteractArea);
}

void AMerchantBase::BeginPlay()
{
	Super::BeginPlay();
}

void AMerchantBase::OverlapInteractArea(UPrimitiveComponent* overlappedComponent, AActor* otherActor,
	UPrimitiveComponent* otherComponent, int otherBodyIndex, bool fromSweep, const FHitResult& hitResult)
{
	if(!SoldOut)
	{
		var player = dynamic_cast<APlayerBase*>(otherActor);
		if (player)
		{
			player->StartShopping(this);
		}
	}
}

void AMerchantBase::BuyItem(APlayerBase* player)
{
	if (SoldOut || player->GetMoney() < Cost) return;

	player->SpendMoney(Cost);
	SoldOut = true;
	OnBuyItem(player);
}
