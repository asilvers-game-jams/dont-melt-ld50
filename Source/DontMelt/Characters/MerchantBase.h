

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../General/CoreGeneral.h"

#include "MerchantBase.generated.h"

class APlayerBase;
class USphereComponent;
UCLASS()
class DONTMELT_API AMerchantBase : public AActor
{
	GENERATED_BODY()
	
public:	
	AMerchantBase();

	UFUNCTION()
	void OverlapInteractArea(UPrimitiveComponent* overlappedComponent, AActor* otherActor, UPrimitiveComponent* otherComponent, int otherBodyIndex, bool fromSweep, const FHitResult& hitResult);

	void BuyItem(APlayerBase* player);

protected:
	virtual void BeginPlay() override;
	
	UFUNCTION(BlueprintImplementableEvent, Category="Events")
	void OnBuyItem(APlayerBase* player);

public:	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USphereComponent* InteractArea;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Display")
	UMaterialInterface* Image;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Display")
	FString Text;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Shopping")
	int Cost;

	UPROPERTY(BlueprintReadOnly, Category="Shopping")
	bool SoldOut {false};
};
