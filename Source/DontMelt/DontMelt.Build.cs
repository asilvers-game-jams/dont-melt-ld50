using UnrealBuildTool;

public class DontMelt : ModuleRules
{
	public DontMelt(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[]
		{
			"Core", "CoreUObject", "Engine", "InputCore",				// Core
			"UMG",														// UI
		});

		PrivateDependencyModuleNames.AddRange(new string[]
		{
			"SlateCore", "Slate"								// UI
		});

		CppStandard = CppStandardVersion.Cpp17;
	}
}
